# Python Example Eventstreams

Example to use kafka-python with the IBM Event Streams, a serviced Kafka.
See the notebook for a working example.

Example to use [kafka-python](https://pypi.org/project/kafka-python/) for [IBM EventStreams](https://cloud.ibm.com/docs/EventStreams/index.html#getting_started)

